from django.db import models
from django.utils.timezone import now

# Create your models here.
class FirstModel(models.Model):
    title = models.CharField(max_length=256, default='666', verbose_name= '標題')
    GENDER_CHOICE = (
        ('1', '男性'),
        ('2','女性')
    )
    gender = models.CharField(max_length=8, default='1', choices= GENDER_CHOICE, verbose_name='性別')
    content = models.TextField(default='', verbose_name='內容', blank= True)
    
    show_status= models.BooleanField(default=False, verbose_name= '是否展示')
    
    height = models.SmallIntegerField(default=180, verbose_name='身高(cm)')
    read_num = models.IntegerField(default=0, verbose_name='閱讀量')
    demo = models.BigIntegerField(default=10000, verbose_name='大整數')
    float_num = models.FloatField(default=0.001, verbose_name='浮點數')
    decimal_num = models.DecimalField(default=0, max_digits=8, decimal_places=3, verbose_name='十進置高精度浮點數')

    date_demo=models.DateField(default=now, verbose_name="date日期")
    time_demo = models.TimeField(default=now, verbose_name="time時間")
    deattime_demo = models.DateTimeField(default=now, verbose_name="datetime日期")

    # 表格的時間自斷再表格的設計之初就要加上
    join_time = models.DateTimeField(auto_now_add=True, verbose_name="加入時間")
    update_time = models.DateTimeField(auto_now=True, verbose_name="更新時間")
    demo_time = models.DateTimeField(auto_now=True, verbose_name="測試時間")

    person_link = models.URLField(default='', blank=True, verbose_name='個人連結')
    person_email = models.EmailField(default='', verbose_name='信箱')

    # 靜態資源[css、js]和媒體資源都不是和網站源碼放在一起,並且網站不提供加載文件服務，文件響應資源都由文件託管服務器提供
    # 開發環境中，django提供資源加載服務
    # 部屬的話要分開
    person_img = models.ImageField(default='', blank= True, verbose_name='個人頭像', upload_to = 'person_image/%Y/%m/%d')
    person_file = models.FileField(default='', verbose_name='私人文件', upload_to='person_file/%Y/%m/%d')
    select_file = models.FilePathField(path='media_file', verbose_name='選擇文件', default='', blank=True, recursive=True, match='.*?jpg') # recursive=True探索文件夾, match='.*?jpg' 正則匹配

    def __str__(self):
        # return f'ID: {self.id}, title: {self.title}'
        return 'ID: {}, Title: {}, show_status: {}'.format(self.id, self.title, self.show_status)
    

# OneToOne 一對一 [人和身分證]
# ForeignKey 多對一 [作者和文章]
# ManyToManyField 多對多 [作者和書籍]

# 多對一    
class TestUser(models.Model):
    nickname = models.CharField(max_length=64, default='暱名', verbose_name='暱稱')
    person_img = models.ImageField(default='', blank=True, verbose_name = '個人頭像', upload_to='person_image/%Y/%m/%d')

    def __str__(self):
        return '暱稱: {}'.format(self.nickname)
    
class TestBlog(models.Model):
    title = models.CharField(max_length=64, default='', blank=True, verbose_name='標題')
    context = models.TextField(verbose_name='內容')
    create_time = models.DateTimeField(default=now, verbose_name='創建時間')
    user = models.ForeignKey(to=TestUser, on_delete=models.CASCADE, related_name='testblogs') # 意味著你可以使用 testblogs 來查詢與 TestUser 相關的所有博客

    # models.CASCADE # 級聯刪除
    # models.SET_DEFAULT # 設置成默認
    # models.SET_NULL # 設置成null
    # models.SET(callback) # callback是可自訂義的對象，需要被調用
    # models.PROTECT #會報錯
    # models.DO_NOTHING # 會報錯

    def __str__(self):
        return '標題: {}, 作者: {}'.format(self.title, self.user)

# 一對一
class TestIDCard(models.Model):
    address = models.CharField(max_length=256, verbose_name='地址')
    number = models.CharField(max_length=18, verbose_name='身分證字號')
    user = models.OneToOneField(to=TestUser, on_delete=models.CASCADE, related_name='idcard')
    # TestUser對象.idcard，就可直接拿到TestIDCard對象
    # TestIDCard對象.user，就可以直接拿到TestUser對象

    def __str__(self):
        return '暱稱: {}, 身分證字號: {}'.format(self.user.nickname, self.number[0:5]+'xxxxxx')

# 多對多
class TestBook(models.Model):
    name = models.CharField(max_length=64, verbose_name='書名')
    users = models.ManyToManyField(to=TestUser, related_name='books')

    def __str__(self):
        return '書名: {}, 作者: {}'.format(self.name, [user.nickname for user in self.users.all()])