# Generated by Django 4.2.2 on 2024-03-11 02:49

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("firstapp", "0002_firstmodel_gender_alter_firstmodel_title"),
    ]

    operations = [
        migrations.AddField(
            model_name="firstmodel",
            name="content",
            field=models.TextField(blank=True, default="", verbose_name="內容"),
        ),
        migrations.AlterField(
            model_name="firstmodel",
            name="gender",
            field=models.CharField(
                choices=[("1", "男性"), ("2", "女性")],
                default="1",
                max_length=8,
                verbose_name="性別",
            ),
        ),
    ]
