# Generated by Django 4.2.2 on 2024-03-11 03:00

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("firstapp", "0004_firstmodel_show_status"),
    ]

    operations = [
        migrations.AddField(
            model_name="firstmodel",
            name="demo",
            field=models.BigIntegerField(default=10000, verbose_name="大整數"),
        ),
        migrations.AddField(
            model_name="firstmodel",
            name="height",
            field=models.SmallIntegerField(default=180, verbose_name="身高(cm)"),
        ),
        migrations.AddField(
            model_name="firstmodel",
            name="read_num",
            field=models.IntegerField(default=0, verbose_name="閱讀量"),
        ),
    ]
