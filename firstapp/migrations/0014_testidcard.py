# Generated by Django 4.2.2 on 2024-03-11 09:47

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        ("firstapp", "0013_testuser_alter_firstmodel_select_file_testblog"),
    ]

    operations = [
        migrations.CreateModel(
            name="TestIDCard",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("address", models.CharField(max_length=256, verbose_name="地址")),
                ("number", models.CharField(max_length=18, verbose_name="身分證字號")),
                (
                    "user",
                    models.OneToOneField(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="idcard",
                        to="firstapp.testuser",
                    ),
                ),
            ],
        ),
    ]
