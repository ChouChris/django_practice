from django.contrib import admin
from .models import FirstModel, TestUser, TestBlog, TestIDCard, TestBook

# Register your models here.
admin.site.register(FirstModel)
admin.site.register(TestUser)
admin.site.register(TestBlog)
admin.site.register(TestIDCard)
admin.site.register(TestBook)